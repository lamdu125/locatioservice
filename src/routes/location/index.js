'use strict'

const express = require('express');
const driverLocationController = require('../../controllers/driverLocation.controller');
const router = express.Router();

//updateLocation
router.post('/driver-location/update', driverLocationController.updateLocation);

//findDriver
router.get('/driver-location/nearby-drivers', driverLocationController.nearbyDrivers)

//find location
router.get('/location/geocoding', driverLocationController.geoCoding);

module.exports = router