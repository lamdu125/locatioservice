'use strict'

const axios = require("axios");
const driverLocationModel = require("../models/driverLocation.model");
const { getInfoData } = require("../utils");
const maxDistance = 10000;

const accessTokenMapbox = 'pk.eyJ1IjoibGFtZHUxMjUiLCJhIjoiY2xseG9xaDBkMWN4aTNpbzl4Y3VvOHU5cSJ9.25lpKlLI5Yo1ZCafisok0g';


class DriverLocationService {
    static saveLocation = async ({ driver_id, coordinates }) => {
        try {
            const existDriver = await driverLocationModel.findOne({ driver_id }).lean();
            if (existDriver) {
                return await driverLocationModel.findOneAndUpdate(
                    { driver_id: driver_id },
                    { $set: { 'currentLocation.coordinates': coordinates } },
                    { new: true }
                )
            }
            const newLocation = await driverLocationModel.create({
                driver_id,
                currentLocation: {
                    type: 'Point',
                    coordinates
                }
            })
            if (newLocation) {
                return {
                    code: 201,
                    metadata: {
                        location: getInfoData({ fields: ['driver_id', 'currentLocation'], object: newLocation }),
                    }
                }
            }
            return {
                code: 201,
                metadata: null
            }
        }
        catch (error) {
            return {
                code: '501',
                message: error.message,
                status: 'error'
            }
        }
    }
    static nearbyDrivers = async ({ userCoordinates }) => {
        try {
            const drivers = await driverLocationModel.find({
                currentLocation: {
                    $near: {
                        $geometry: {
                            type: 'Point',
                            coordinates: userCoordinates
                        },
                        $maxDistance: maxDistance
                    }
                }
            }).limit(5)

            if (drivers.length) {
                return {
                    code: 200,
                    metadata: {
                        driver: drivers.map(
                            driver => getInfoData({ fields: ['driver_id', 'currentLocation'], object: driver })
                        )
                    }
                }
            }
            return {
                code: 200,
                metadata: null
            }

        } catch (error) {
            return {
                code: '501',
                message: error.message,
                status: 'error'
            }
        }
    }

    static geoCoding = async ({ query }) => {

        const apiMapboxGeo = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(query)}.json`;
        const params = {
            access_token: accessTokenMapbox,
            limit: 1
        }
        try {

            const response = await axios.get(apiMapboxGeo, { params });

            if (response && response.data && response.data.features && response.data.features.length > 0) {
                const result = response.data.features[0];
                const { center, place_name } = result;
                return {
                    code: 200,
                    metadata: {
                        location: {
                            longitude: center[0],
                            latitude: center[1],
                            placeName: place_name
                        }
                    }
                }
            }
            return {
                code: 200,
                metadata: null
            }

        } catch (error) {
            return {
                code: '501',
                message: error.message,
                status: 'error'
            }
        }
    }
}

module.exports = DriverLocationService