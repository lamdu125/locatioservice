'use strict'

const driverLocationService = require("../services/driverLocation.service")

class DriverLocationController {
    updateLocation = async (req, res, next) => {
        try {
            console.log(`[P]::updateLocation`, req.body)
            return res.status(201).json(await driverLocationService.saveLocation(req.body))
        }
        catch (error) {
            next(error)
        }
    }

    nearbyDrivers = async (req, res, next) => {
        try {
            console.log(`[P]::findDrivers`, req.body);

            if (!req.body || !req.body.userCoordinates) {
                return res.status(400).json({ message: "Missing required parameters in the request body." });
            }

            const drivers = await driverLocationService.nearbyDrivers(req.body);

            if (!drivers) {
                return res.status(404).json({ message: "No drivers found nearby." });
            }

            return res.status(200).json(drivers);
        } catch (error) {
            console.error("Error fetching nearby drivers:", error.message);
            next(error)
        }
    }
    geoCoding = async (req, res, next) => {
        try {
            console.log(`[P]::findLocation`, req.body);
            if (!req.body || !req.body.query) {
                return res.status(400).json({ message: "Missing required parameters in the request body." });
            }

            const location = await driverLocationService.geoCoding(req.body);

            if (!location) {
                return res.status(404).json({ message: "No location found." });
            }

            return res.status(200).json(location);
        } catch (error) {
            console.error("Error fetching location:", error.message);
            next(error)
        }
    }
}

module.exports = new DriverLocationController()